python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/train.txt \
    --output_path ../dataset/CCPD2019/det_gt.txt

python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/train.txt \
    --output_path ../dataset/CCPD2019/det_gt_train.txt

python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/train_min.txt \
    --output_path ../dataset/CCPD2019/det_gt_train_min.txt

python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/val.txt \
    --output_path ../dataset/CCPD2019/det_gt_val.txt

python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/val_min.txt \
    --output_path ../dataset/CCPD2019/det_gt_val_min.txt

python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/test.txt \
    --output_path ../dataset/CCPD2019/det_gt_test.txt

python mindocr/tools/dataset_converters/convert.py \
    --dataset_name ccpd --task det \
    --image_dir ../dataset/CCPD2019/ \
    --label_dir ../dataset/CCPD2019/splits/test_min.txt \
    --output_path ../dataset/CCPD2019/det_gt_test_min.txt

python tools/eval.py -c=configs/det/dbnet/db_r50_icdar15.yaml \
          --opt eval.ckpt_load_path="/home/chenzhenling/remote_develop/mindocr_project2/mindocr/tmp_det/best.ckpt" \
                eval.dataset_root="/home/chenzhenling/remote_develop/dataset/data/val/" \
                eval.data_dir="validation/"\
                eval.label_file="det_gt_val.txt"

python tools/eval.py -c=configs/det/dbnet/db_r50_icdar15.yaml --opt eval.ckpt_load_path="/home/chenzhenling/remote_develop/mindocr_project2/mindocr/tmp_det/best.ckpt"   eval.dataset_root="/home/chenzhenling/remote_develop/dataset/data/val/"   eval.data_dir="validation/"  eval.label_file="det_gt_val.txt"

python tools/export.py --model_name_or_config dbnet_resnet50 --data_shape 720 1160 --local_ckpt_path /home/chenzhenling/remote_develop/mindocr_project2/mindocr/tmp_det/best.ckpt
# 使用本地ckpt文件，导出`DBNet ResNet-50 en` 模型的MindIR
# 更多参数使用详情，请执行 `python tools/export.py -h`
python tools/export.py --model_name_or_config dbnet_resnet50 --data_shape 736 1280 --local_ckpt_path /path/to/dbnet.ckpt
python tools/export.py --model_name_or_config /home/chenzhenling/remote_develop/mindocr_project2/mindocr/configs/det/dbnet/db_r50_icdar15.yaml --data_shape 720 1160 --local_ckpt_path /home/chenzhenling/remote_develop/mindocr_project2/mindocr/tmp_det/best.ckpt


python infer.py \
    --input_images_dir=/home/chenzhenling/remote_develop/dataset/data_min/test/testing/ \
    --device=Ascend \
    --device_id=0 \
    --det_model_path=/home/chenzhenling/remote_develop/mindocr_project2/mindocr/dbnet_resnet50.mindir \
    --det_model_name_or_config=../../configs/det/dbnet/db_r50_icdar15.yaml \
    --backend=lite \
    --res_save_dir=results_dir

converter_lite \
    --saveType=MINDIR \
    --fmk=MINDIR \
    --optimize=ascend_oriented \
    --modelFile=/home/chenzhenling/remote_develop/mindocr_project2/mindocr/db_r50_icdar15.mindir \
    --outputFile=dbnet_resnet50

export LD_LIBRARY_PATH=/home/chenzhenling/remote_develop/mslite/tools/converter/lib:${LD_LIBRARY_PATH}

python infer.py \
    --input_images_dir=/home/chenzhenling/remote_develop/dataset/data_min/test/testing/ \
    --det_model_path=/home/chenzhenling/remote_develop/mindocr_project2/mindocr/dbnet_resnet50.mindir \
    --det_model_name_or_config=../../configs/det/dbnet/db_r50_icdar15.yaml \
    --res_save_dir=/home/chenzhenling/remote_develop/mindocr_project2/mindocr/my_result


python deploy/eval_utils/eval_det.py \
		--gt_path=/path/to/ic15/test_det_gt.txt \
		--pred_path=/path/to/dbnet_resnet50_results/det_results.txt

