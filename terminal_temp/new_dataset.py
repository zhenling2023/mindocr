import sys
import os
import shutil


def copyFile(fileList, targetDir):
    log = open('running.log', 'w')
    with open(fileList) as f:
        for line in f.readlines():
            line = line.strip('\r\n')
            if line.__len__() == 0:
                continue
            line = "/home/chenzhenling/remote_develop/dataset/CCPD2019/" + line
            basename = os.path.basename(line)
            print(basename)
            exists = os.path.exists(line)
            if exists:
                fullPath = targetDir + '/' + basename
                print('Copy %s to %s' % (line, fullPath))
                log.write('Copy %s to %s \r\n' % (line, fullPath))
                shutil.copy(line, targetDir + '/' + basename)
            else:
                print("%s not exists" % line)
                log.write("%s not exists \r\n" % line)
    log.close()


if __name__ == '__main__':
    # fileList = 'filelist.txt'
    # targetDir = 'NewFolder'
    # fileList = "/home/chenzhenling/remote_develop/dataset/CCPD2019/splits/train.txt"
    # targetDir = "/home/chenzhenling/remote_develop/dataset/data/train/training/"

    # fileList = "/home/chenzhenling/remote_develop/dataset/CCPD2019/splits/val.txt"
    # targetDir = "/home/chenzhenling/remote_develop/dataset/data/val/validation/"
    #
    # fileList = "/home/chenzhenling/remote_develop/dataset/CCPD2019/splits/test.txt"
    # targetDir = "/home/chenzhenling/remote_develop/dataset/data/test/testing/"

    # fileList = "/home/chenzhenling/remote_develop/dataset/CCPD2019/splits/train_min.txt"
    # targetDir = "/home/chenzhenling/remote_develop/dataset/data_min/train/training/"

    # fileList = "/home/chenzhenling/remote_develop/dataset/CCPD2019/splits/val_min.txt"
    # targetDir = "/home/chenzhenling/remote_develop/dataset/data_min/val/validation/"

    fileList = "/home/chenzhenling/remote_develop/dataset/CCPD2019/splits/test_min.txt"
    targetDir = "/home/chenzhenling/remote_develop/dataset/data_min/test/testing/"
    if not os.path.exists(targetDir):
        os.makedirs(targetDir)
    copyFile(fileList, targetDir)
